# NeuralNetIMGProject
Dieses Projekt ist eine private Studie von Neuronalen Netzten. Dabei wird zum trainieren der neuronalen Netzte backpropagation verwendet.
Es ist umgesetzt in Java und OpenJavaFX.

Das Programm kann mit maven gestartet werden: `mvn clean javafx:run`

Bereits trainierte Netze können unter Networks geladen werden.

![UI-Image](image.png)

Disclaimer:
Dies ist ein reines Lehr-Projekt welches ich im dritten Semester des Informatik-Studiums erstellt habe, erwartet also keinen Lupenreinen Code ;)
