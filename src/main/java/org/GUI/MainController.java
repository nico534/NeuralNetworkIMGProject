package org.GUI;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.io.IOException;

public class MainController {
    private Button changeToTrain;
    private Button changeToPredict;

    private BorderPane window;
    private HBox menu;
    private Parent predScene;
    private Parent trainScene;

    public MainController() throws IOException {
        window = new BorderPane();
        changeToTrain = new Button("train");
        changeToTrain.setOnAction(e -> {window.setCenter(trainScene); changeToTrain.setDisable(true); changeToPredict.setDisable(false);});
        changeToPredict = new Button("predict");
        changeToPredict.setOnAction(e -> {window.setCenter(predScene); changeToTrain.setDisable(false); changeToPredict.setDisable(true);});
        changeToPredict.setPrefWidth(100);
        changeToTrain.setPrefWidth(100);
        menu = new HBox();
        menu.getChildren().addAll(changeToPredict, changeToTrain);

        window.setTop(menu);

        predScene = FXMLLoader.load(getClass().getResource("predictionScene.fxml"));
        trainScene = FXMLLoader.load(getClass().getResource("trainingScene.fxml"));

        changeToPredict.setDisable(true);
        window.setCenter(predScene);
    }

    public BorderPane getWindow(){
        return window;
    }
}
