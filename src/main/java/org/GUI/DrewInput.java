package org.GUI;


import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.paint.Color;

public class DrewInput extends Canvas {

    private GraphicsContext gc;
    private int width;
    private int height;
    private int thickness;
    private double smoothness;

    private double drowX;
    private double drowY;

    public DrewInput(int width, int height){
        super(width, height);
        this.thickness = 30;
        this.smoothness = 0.1;

        this.width = width;
        this.height = height;
        gc = getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        // fill with black
        gc.fillRect(0, 0, width, height);
        gc.fill();

        addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                gc.setFill(Color.WHITE);
                gc.setGlobalAlpha(smoothness);
                gc.fillOval(mouseEvent.getX() - (thickness/2), mouseEvent.getY() - (thickness/2), thickness, thickness);
                drowX = mouseEvent.getX() - (thickness/2);
                drowY = mouseEvent.getY() - (thickness/2);
                gc.stroke();
            }
        });
        addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                gc.setFill(Color.WHITE);
                drowTo(mouseEvent.getX()-(thickness/2), mouseEvent.getY() - (thickness/2));
                gc.stroke();
            }
        });

        addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                gc.setFill(Color.BLACK);
            }
        });
    }

    public void clear(){
        gc.setGlobalAlpha(1.0);
        gc.fillRect(0, 0, width, height);
    }

    public void drowTo(double toX, double toY){
        double normX = drowX - toX;
        double normY = drowY - toY;
        double length = Math.sqrt(Math.pow(normX,2) + Math.pow(normY,2));
        normX *= (1.0 /length);
        normY *= (1.0/length);
        for(int i = 0; i < length; i++){
            gc.fillOval(drowX + ((int)(normX*i)),drowY + ((int)(normY*i)), thickness-(2*i), thickness-(2*i));

            gc.fillOval(drowX + (Math.ceil(normX*i)),drowY + (Math.ceil(normY*i)), thickness-(2*i), thickness-(2*i));
        }
        drowX = toX;
        drowY = toY;
    }
}
