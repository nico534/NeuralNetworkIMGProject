import static org.junit.jupiter.api.Assertions.assertEquals;

import org.NeuralNet.NeuralNetwork;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.Matrixes.Matrix;

import java.io.File;
import java.io.IOException;

public class NeuralNetTest {

    @Test
    @DisplayName("NN Durchlauf")
    void nnDurchlaufTest() {
        float[][] inputs = {{2.0f, 1.5f},{2, 1},{4, 1.5f},{3, 1},{3.5f, .5f},{2, .5f},{5.5f, 1},{1, 1}};
        float[][] labels = {{0, 1}, {1, 0}, {0, 1}, {1, 0}, {0, 1}, {1, 0}, {0, 1}, {1, 0}};

        Matrix[] example = new Matrix[inputs.length];
        Matrix[] exLabels = new Matrix[labels.length];

        for(int i = 0; i < inputs.length; i++){
            example[i] = new Matrix(inputs[i]);
            exLabels[i] = new Matrix(labels[i]);
        }
        int[] layerSize = {2, 3, 2};
        NeuralNetwork nn = new NeuralNetwork(layerSize);

        nn.train(example, exLabels, 0.2, true);
    }

    @Test
    @DisplayName("Save load")
    void saveLoadTest() throws IOException {

        int[] layerSize = {2, 3, 2};
        NeuralNetwork nn = new NeuralNetwork(layerSize);
        File f = new File("F:/Bibliothek/Desktop/Snake/test.csv");
        nn.saveNeuralNetwork(f);

        NeuralNetwork nn2 = new NeuralNetwork(f);

        Matrix[] weights1 = nn.getWeights();
        Matrix[] weights2 = nn2.getWeights();

        for(int i = 0; i < weights1.length; i++){
            assertEquals(weights1[i].getRows(), weights2[i].getRows());
            assertEquals(weights1[i].getCols(), weights2[i].getCols());
            for(int j = 0; j < weights1[i].getRows(); j++){
                for(int k = 0; k < weights1[i].getCols(); k++){
                    assertEquals(weights1[i].get(j, k), weights2[i].get(j, k));
                }
            }
        }

        Matrix[] bias1 = nn.getBiases();
        Matrix[] bias2 = nn2.getBiases();

        for(int i = 0; i < bias1.length; i++){
            assertEquals(bias1[i].getRows(), bias2[i].getRows());
            assertEquals(bias1[i].getCols(), 1);
            assertEquals(bias2[i].getCols(), 1);
            for(int j = 0; j < bias1[i].getRows(); j++){
                assertEquals(bias1[i].get(j), bias2[i].get(j));
            }
        }
    }
}