package org.GUI;

import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.Matrixes.Matrix;
import org.NeuralNet.NeuralNetwork;


import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class PredictionControler implements Initializable {

    public Label predNr;
    public Button openNetwork;
    public Label plausibility;
    public Label networkName;
    public Pane drawPane;
    private File dataFile;

    private boolean alreadyAddet;

    private NeuralNetwork nn;
    private String predictedImg;

    private DrewInput drawInput = new DrewInput(280, 280);

    public void openNewNetwork() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open neural network");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File nnFile;
        Stage openStage = new Stage();
        openStage.initModality(Modality.APPLICATION_MODAL);
        nnFile = fileChooser.showOpenDialog(openStage);
        if (nnFile != null) {
            try {
                this.nn = new NeuralNetwork(nnFile);
                networkName.setText(nnFile.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void predictDrawnImage() {
        if (nn == null) {
            return;
        }
        WritableImage img = drawInput.snapshot(null, null);
        BufferedImage bImg = new BufferedImage(280, 280, BufferedImage.TYPE_INT_ARGB);
        for (int i = 0; i < img.getHeight(); i++) {
            for (int j = 0; j < img.getWidth(); j++) {
                int rgb = img.getPixelReader().getArgb(i, j);
                bImg.setRGB(i, j, rgb);
            }
        }
        BufferedImage smallImg = new BufferedImage(28, 28, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = smallImg.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(bImg, 0, 0, 28, 28, 0, 0, bImg.getHeight(), bImg.getWidth(), null);
        g.dispose();

        predictedImg = "";
        WritableRaster raster = smallImg.getRaster();
        float[] toPredict = new float[784];
        int counter = 0;
        for (int i = 0; i < raster.getHeight(); i++) {
            for (int j = 0; j < raster.getWidth(); j++) {
                int[] intA = new int[4];
                int[] erg = raster.getPixel(j, i, intA);
                predictedImg += erg[0] + ",";
                toPredict[counter] = erg[0] / 255f;
                counter++;
            }
        }
        predictedImg = predictedImg.substring(0, predictedImg.length() - 1);
        int erg = nn.predict(new Matrix(toPredict));
        System.out.println(erg);
        predNr.setText(erg + "");
        plausibility.setText((Math.round(nn.getPredictedProb() * 100)) + "%");
        alreadyAddet = false;
    }

    public void clearImage() {
        drawInput.clear();
    }

    public void rightAnswer() {
        if (!alreadyAddet) {
            addToLibrarry(predNr.getText());
        }
    }

    public void zero() {
        if (!alreadyAddet) {
            addToLibrarry("0");
        }
    }

    public void one() {
        if (!alreadyAddet) {
            addToLibrarry("1");
        }
    }

    public void two() {
        if (!alreadyAddet) {
            addToLibrarry("2");
        }
    }

    public void three() {
        if (!alreadyAddet) {
            addToLibrarry("3");
        }
    }

    public void four() {
        if (!alreadyAddet) {
            addToLibrarry("4");
        }
    }

    public void five() {
        if (!alreadyAddet) {
            addToLibrarry("5");
        }
    }

    public void six() {
        if (!alreadyAddet) {
            addToLibrarry("6");
        }
    }

    public void seven() {
        if (!alreadyAddet) {
            addToLibrarry("7");
        }
    }

    public void eight() {
        if (!alreadyAddet) {
            addToLibrarry("8");
        }
    }

    public void nine() {
        if (!alreadyAddet) {
            addToLibrarry("9");
        }
    }

    private void addToLibrarry(String value) {
        String toSave = value + "," + predictedImg;
        try (FileWriter fx = new FileWriter(dataFile, true);
             BufferedWriter bw = new BufferedWriter(fx);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(toSave);
        } catch (IOException e) {
            e.printStackTrace();
        }
        alreadyAddet = true;
        clearImage();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        drawPane.getChildren().add(drawInput);
        dataFile = new File("TrainLibrarys/own_data.csv");
    }
}
