package org.GUI;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        MainController c = new MainController();
        primaryStage.setTitle("Neural Network");
        primaryStage.setScene(new Scene(c.getWindow(), 600, 430));
        primaryStage.show();
    }
}
