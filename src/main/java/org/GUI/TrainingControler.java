package org.GUI;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javafx.util.Duration;
import org.NeuralNet.NNTraining;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class TrainingControler implements Initializable {
    private File TEST_DATA;
    private File OWN_TEST_DATA;

    public Button startStopButton;
    public Button saveButton;
    public Button createNet;

    public TextField hiddenLayerInput;
    public Button loadNetwork;
    public Slider learningRate;
    public Slider learningNr;

    public Label errorOutput;
    public Label accuracy;
    public Label showLearningNr;
    public Label showLearningRate;
    public CheckBox useOwnTrainingData;
    public LineChart<Number, Number> graph;
    private ArrayList<Double> graphValues = new ArrayList<>();

    private File nnFile;
    private NNTraining nn = new NNTraining();

    private boolean running = false;
    private boolean firstCole = false;

    KeyFrame update2 = new KeyFrame(Duration.seconds(0.5), event -> {
        loadNetwork.setDisable(false);
        saveButton.setDisable(false);
        startStopButton.setText("Start");
    });
    Timeline t2 = new Timeline(update2);

    private Service<String> trainingTask = new Service<String>() {
        @Override
        protected Task<String> createTask() {
            return new Task<String>() {
                @Override
                protected String call() throws IOException {
                    System.out.println("start training");
                    startTraining();
                    return null;
                }
            };
        }
    };

    public void startStopKlicked() {
        t2.pause();
        if (nn == null) {
            errorOutput.setText("No Network available");
            return;
        }
        if (running) {
            running = false;
            startStopButton.setText("Start");
        } else {
            if (!trainingTask.isRunning()) {
                trainingTask.restart();
                startStopButton.setText("Stop");
                loadNetwork.setDisable(true);
                saveButton.setDisable(true);
            }
        }
    }


    public void loadNewNetwork() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open neural network");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        if (nnFile != null && nnFile.exists()) {
            fileChooser.setInitialDirectory(nnFile.getParentFile());
        }
        Stage openStage = new Stage();
        openStage.initModality(Modality.APPLICATION_MODAL);
        nnFile = fileChooser.showOpenDialog(openStage);
        if (nnFile != null && nnFile.isFile()) {
            this.nn.setTrainingFile(nnFile);
        }

    }

    public void saveNeuralNetwork() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open neural network");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        if (nnFile != null && nnFile.exists()) {
            fileChooser.setInitialDirectory(nnFile.getParentFile());
        }
        Stage saveStage = new Stage();
        saveStage.initModality(Modality.APPLICATION_MODAL);
        nnFile = null;
        nnFile = fileChooser.showSaveDialog(saveStage);
        System.out.println(nnFile != null);
        if (nnFile != null) {
            try {
                System.out.println("Save File " + nnFile.getName());
                nn.getNN().saveNeuralNetwork(nnFile);
            } catch (IOException e) {
                errorOutput.setText("Error, cant save the Network.");
            }
        }
    }


    private void startTraining() throws IOException {
        if (!useOwnTrainingData.isSelected()) {
            this.nn.loadTestData(TEST_DATA);
        } else {
            this.nn.loadTestData(OWN_TEST_DATA);
        }
        if (!this.nn.setUp()) {
            System.out.println("Can't setup");
            t2.play();
            return;
        }
        int range = (int) learningNr.getValue();
        if (nn.getTestDataLength() == 0) {
            System.out.println("test data set is empty.");
            t2.play();
            return;
        } else if (range > nn.getTestDataLength() - 1) {
            range = nn.getTestDataLength() - 1;
        }
        this.graphValues.clear();
        this.running = true;
        double learningR = learningRate.getValue();
        long time = System.currentTimeMillis();
        while (running) {
            if (System.currentTimeMillis() - time > 1000) {
                nn.startTestRoutine(range, learningR, true);
                firstCole = true;
                KeyFrame update = new KeyFrame(Duration.seconds(0.5), event -> {
                    if (firstCole) {
                        firstCole = false;
                    } else {
                        return;
                    }
                    accuracy.setText(Math.round(nn.getNN().getErrorRate()) + "%");
                    addToLineChart(nn.getNN().getErrorRate());
                });
                Timeline t1 = new Timeline(update);
                t1.setCycleCount(Timeline.INDEFINITE);
                t1.play();
                time = System.currentTimeMillis();
            } else {
                nn.startTestRoutine(range, learningR, false);
            }
        }
        t2.play();
    }

    public void addToLineChart(double number) {
        if (this.graphValues.size() < 100) {
            this.graphValues.add(number);
        } else {
            this.graphValues.remove(0);
            this.graphValues.add(number);
        }

        graph.getData().clear();
        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        for (int i = 0; i < this.graphValues.size(); i++) {
            series.getData().add(new XYChart.Data<>(i, this.graphValues.get(i)));
        }
        graph.getData().add(series);
    }

    public void testInput() {
        if (hiddenLayerInput.getText() == null) {
            createNet.setDisable(true);
            return;
        }
        if (hiddenLayerInput.getText().matches("(([1-9]\\d*,)*)([1-9]\\d*)")) {
            hiddenLayerInput.setStyle("-fx-text-inner-color: black;");
            createNet.setDisable(false);
        } else {
            hiddenLayerInput.setStyle("-fx-text-inner-color: red;");
            createNet.setDisable(true);
        }
    }

    public void createNewNetwork() {
        String[] hiddenLayerStr = hiddenLayerInput.getText().split(",");
        int[] hiddenLayerSize = new int[hiddenLayerStr.length];
        for (int i = 0; i < hiddenLayerSize.length; i++) {
            hiddenLayerSize[i] = Integer.parseInt(hiddenLayerStr[i]);
        }
        nn.setHiddenLayerse(hiddenLayerSize);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        learningNr.valueProperty().addListener((obs, oldval, newval) -> learningNr.setValue(newval.intValue()));
        showLearningNr.textProperty().bindBidirectional(learningNr.valueProperty(), NumberFormat.getNumberInstance());
        showLearningRate.textProperty().bindBidirectional(learningRate.valueProperty(), NumberFormat.getNumberInstance());
        createNet.setDisable(true);
        t2.setCycleCount(Timeline.INDEFINITE);

        TEST_DATA = new File("TrainLibrarys/mnist_train.csv");
        OWN_TEST_DATA = new File("TrainLibrarys/own_data.csv");
        graph.setCreateSymbols(false);
        graph.setLegendVisible(false);
        graph.setAnimated(false);
        graph.setVerticalZeroLineVisible(false);
    }

    public void clearNetwork() {
        t2.pause();
        nn.clear();
    }
}
