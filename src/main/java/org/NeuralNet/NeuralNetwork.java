package org.NeuralNet;

import org.Matrixes.*;
import org.apache.commons.lang3.ArrayUtils;
import java.io.*;

public class NeuralNetwork {

    private static final String DEFAULT_SEPERATOR = ",";

    private Matrix[] weights;
    private Matrix[] biases;
    private int layerCount;
    private double errorRate;

    private double predictedProb;

    public NeuralNetwork(int[] layerSize) {
        init(layerSize, true);
    }

    public NeuralNetwork(File networFile) throws IOException {
        loadNeuralNetworkFromFile(networFile);
    }

    private void init(int[] layerSize, boolean randomize){
        this.layerCount = layerSize.length;
        this.weights = new Matrix[layerCount - 1];
        this.biases = new Matrix[layerCount - 1];
        for (int i = 0; i < layerCount - 1; i++) {
            this.weights[i] = new Matrix(layerSize[i + 1], layerSize[i], randomize);
            this.biases[i] = new Matrix(layerSize[i + 1], 1, randomize);
        }
    }

    public int predict(Matrix input) {
        for (int i = 0; i < layerCount - 1; i++) {
            input = MatrixCalc.calcPrediction(this.weights[i], input, this.biases[i]);
        }
        this.predictedProb = input.get(input.getMaxIndex());
        return input.getMaxIndex();
    }

    public double getPredictedProb(){
        return this.predictedProb;
    }

    private Matrix[][] feedForward(Matrix[] input) throws InterruptedException {
        Matrix[][] allLayers = new Matrix[this.layerCount][input.length];
        allLayers[0] = input;
        for (int i = 0; i < layerCount - 1; i++) {
            allLayers[i + 1] = MatrixCalc.calcPrediction(this.weights[i], allLayers[i], this.biases[i]);
        }
        return allLayers;
    }

    /**
     * Calculats the Errors of the given predictions and answers per neuronStrang
     *
     * @param predictions - the calculated final predictions
     * @param answers     - the right answers for the predictions
     * @return - all Errors
     * @throws InterruptedException - is in parallel
     */
    private Matrix[][] calcError(Matrix[] predictions, Matrix[] answers, boolean printError) throws InterruptedException {
        if (printError) {
            calculateErrorRate(predictions, answers);
        }

        Matrix[][] errors = new Matrix[this.layerCount - 1][predictions.length];
        for (int i = 0; i < predictions.length; i++) {
            errors[0][i] = answers[i].copy();
            errors[0][i].subtract(predictions[i]);
        }

        for (int i = 1; i < this.layerCount - 1; i++) {
            errors[i] = MatrixCalc.mtxATransposeMtxB(this.weights[this.layerCount - i - 1], errors[i - 1]);
        }
        ArrayUtils.reverse(errors);
        return errors;
    }

    private void calculateErrorRate(Matrix[] predictions, Matrix[] answers) {
        int rate = 0;
        for (int i = 0; i < predictions.length; i++) {
            if (predictions[i].getMaxIndex() == answers[i].getMaxIndex()) {
                rate++;
            }
        }
        this.errorRate = ((double) rate / (double) predictions.length) * 100;
    }


    /**
     * Trains the network with the given parameters
     *
     * @param inputs - input data
     * @param labels - results
     * @param learningRate - learningRate
     * @return - true by success, otherwise false.
     */
    public boolean train(Matrix[] inputs, Matrix[] labels, double learningRate, boolean print) {
        Matrix[][] allPredictions = null;
        Matrix[][] allErrors = null;
        try {
            allPredictions = feedForward(inputs);
            // Calculate the Errors with the last Result of the prediction (the output neurons)
            allErrors = calcError(allPredictions[this.layerCount - 1], labels, print);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
        /* calculate gradiant, multiply by vector and learningRate and add to the biases
           Afterwords multiply gradiant * Prediction.transpose to get delta_W for the Weights */
        for (int i = 0; i < this.layerCount - 1; i++) {
            try {
                Matrix[] nabla_b = MatrixCalc.calcGradiant(allPredictions[i + 1]);
                for (int j = 0; j < nabla_b.length; j++) {
                    nabla_b[j].multiplyValues(allErrors[i][j]);
                }

                Matrix[] nabla_w = MatrixCalc.mtxAMtxBTranspose(nabla_b, allPredictions[i]);
                for (int j = 0; j < nabla_b.length; j++) {
                    nabla_b[j].multiplyScalar(learningRate/inputs.length);
                    this.biases[i].addUp(nabla_b[j]);
                    nabla_w[j].multiplyScalar(learningRate/inputs.length);
                    this.weights[i].addUp(nabla_w[j]);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }


    public void saveNeuralNetwork(File f) throws IOException {
        if(f.exists()){
            f.delete();
        }
        f.createNewFile();
        FileWriter writer = new FileWriter(f);

        System.out.println("Saving the layout...");
        //first line is the Network layout
        String nextLine = "";
        for(int i = 0; i < this.weights.length; i++){
            nextLine += this.weights[i].getCols() + DEFAULT_SEPERATOR;
        }
        nextLine += this.weights[this.weights.length - 1].getRows() + "\n";
        writer.append(nextLine);

        System.out.println("Saving the weight matrices...");
        // AfterWords the WeightMatrixes as Lines
        for(Matrix w: this.weights){
            nextLine = "";
            for(int i = 0; i < w.getRows(); i++){
                for(int j = 0;j < w.getCols(); j++){
                    nextLine += (w.get(i, j) + DEFAULT_SEPERATOR);
                }
                nextLine = nextLine.substring(0, nextLine.length() - 1);
                nextLine += "\n";
                writer.append(nextLine);
                nextLine = "";
            }
        }

        System.out.println("Saving the biases...");
        // and the Biases as Vector lines
        for(Matrix b: this.biases){
            nextLine = "";
            for(int i = 0; i < b.getRows(); i++){
                for(int j = 0;j < b.getCols(); j++){
                    nextLine += (b.get(i, j) + DEFAULT_SEPERATOR);
                }
            }
            nextLine = nextLine.substring(0, nextLine.length() - 1);
            nextLine += "\n";
            writer.append(nextLine);
        }
        writer.flush();
        writer.close();
    }

    private void loadNeuralNetworkFromFile(File f) throws IOException {
        BufferedReader br = null;
        String line = "";
        br = new BufferedReader(new FileReader(f));
        if((line = br.readLine()) == null){
            throw new IOException("file is damaged ore missing");
        }
        String[] seperated = line.split(DEFAULT_SEPERATOR);

        //load LayerSize
        int[] layerSize = new int[seperated.length];

        for(int i = 0; i < seperated.length; i++)
            layerSize[i] = Integer.parseInt(seperated[i]);
        init(layerSize, false);

        //load Weight Matrices
        for(Matrix w: this.weights){
            for(int i = 0; i < w.getRows(); i++) {
                if ((line = br.readLine()) == null) {
                    throw new IOException("file is damaged ore missing");
                }
                seperated = line.split(DEFAULT_SEPERATOR);
                if (seperated.length != w.getCols()) {
                    throw new IOException("file is damaged ore missing");
                }
                for (int j = 0; j < w.getCols(); j++) {
                    w.set(i, j, Float.parseFloat(seperated[j]));
                }
            }
        }

        //load Bias Vectors
        for(Matrix b: this.biases){
            if((line = br.readLine()) == null){
                throw new IOException("file is damaged ore missing");
            }
            seperated = line.split(DEFAULT_SEPERATOR);
            if(seperated.length != b.getRows() * b.getCols()){
                throw new IOException("file is damaged ore missing");
            }
            for(int i = 0; i < b.getRows(); i++){
                b.set(i, 0, Float.parseFloat(seperated[i]));
            }
        }
    }

    public double getErrorRate() {
        return errorRate;
    }

    public Matrix[] getWeights(){
        return weights;
    }

    public Matrix[] getBiases(){
        return biases;
    }
}
