import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.Matrixes.Matrix;

public class MatrixTest {


    @Test
    @DisplayName("Printing")
    public void printTest(){
        Matrix mtx = new Matrix(200, 300, true);
        mtx.print();
        System.out.println();
        mtx = new Matrix(3, 50, true);
        mtx.print();
        System.out.println();
        mtx = new Matrix(200, 4, true);
        mtx.print();
    }

    @Test
    @DisplayName("MaxIndex test")
    public void maxIndexTest(){
        float[] mtxArgs = {4, 2, 53, 55, 76, 3, 5, 4};
        Matrix mtx = new Matrix(mtxArgs);
        assertEquals(mtx.getMaxIndex(), 4);
    }
    @Test
    @DisplayName("Matrix subtraction")
    public void subtractionTest(){
        float[] mtxAVal = {2, 3, 5, 3, 5, 2, 2, 3, 4};
        float[] mtxBVal = {1, 4, 5, 3, 5, 2, 2, 3, 4};

        float[][] testErg = {{1, -1, 0}, {0, 0, 0}, {0, 0, 0}};

        Matrix mtxA = new Matrix(mtxAVal, 3);
        Matrix mtxB = new Matrix(mtxBVal, 3);

        mtxA.subtract(mtxB);
        for(int i = 0; i < mtxA.getRows(); i++){
            for(int j = 0; j < mtxA.getCols(); j++){
                assertEquals(mtxA.get(i, j), testErg[i][j], 0.00001);
            }
        }
    }

    @Test
    @DisplayName("sigmoid test")
    public void sidmoidTest(){
        float[] mtxArgs = {4, -2, 53, 55, -76, 3, 5, 4};
        Matrix mtx = new Matrix(mtxArgs);
        Matrix mtx2 = mtx.copy();
        mtx.sigmoid();

        for(int i = 0; i < mtxArgs.length; i++){
            assertEquals(mtx.get(i), ((float) (1.0 / (1.0 + Math.exp(-mtx2.get(i))))), 0.0000001);
        }
    }

    @Test
    @DisplayName("multiplyScalar test")
    public void multiplyScalarTest(){
        float[] mtxArgs = {4, -2, 53, 55, -76, 3, 5, 4};
        Matrix mtx = new Matrix(mtxArgs);
        Matrix mtx2 = mtx.copy();
        mtx.multiplyScalar(2.0);
        for(int i = 0; i < mtxArgs.length; i++){
            assertEquals(mtx.get(i), 2 * mtx2.get(i), 0.0000001);
        }
    }


    @Test
    @DisplayName("Matrix addition")
    public void additionTest(){
        float[] mtxAVal = {2, 3, 5, 3, 5, 2, 2, 3, 4};
        float[] mtxBVal = {2, 3, 5, 3, 5, 2, 2, 3, 4};

        float[][] testErg = {{4, 6, 10}, {6, 10, 4}, {4, 6, 8}};

        Matrix mtxA = new Matrix(mtxAVal, 3);
        Matrix mtxB = new Matrix(mtxBVal, 3);

        mtxA.addUp(mtxB);
        for(int i = 0; i < mtxA.getRows(); i++){
            for(int j = 0; j < mtxA.getCols(); j++){
                assertEquals(mtxA.get(i, j), testErg[i][j], 0.00001);
            }
        }
    }

    @Test
    @DisplayName("Matrix multiplication")
    public void multiplicationTest(){
        float[] mtxAVal = {2, 3, 5, 3, 5, 2, 2, 3, 4};
        float[] mtxBVal = {2, 3, 5, 3, 5, 2, 2, 3, 4};

        float[][] testErg = {{4, 9, 25}, {9, 25, 4}, {4, 9, 16}};

        Matrix mtxA = new Matrix(mtxAVal, 3);
        Matrix mtxB = new Matrix(mtxBVal, 3);

        mtxA.multiplyValues(mtxB);
        for(int i = 0; i < mtxA.getRows(); i++){
            for(int j = 0; j < mtxA.getCols(); j++){
                assertEquals(mtxA.get(i, j), testErg[i][j], 0.00001);
            }
        }
    }

    @Test
    @DisplayName("Copy Matrix test")
    public void testLinearMultiplication() throws InterruptedException {
        Matrix mtx = new Matrix(5, 5, true);
        Matrix mtxCopy = mtx.copy();
        assertTrue(sameMatrixes(mtx, mtxCopy));
        mtx.set(0, 3, 0);
        assertFalse(sameMatrixes(mtx, mtxCopy));
    }

    @Test
    @DisplayName("setter test")
    public void setterTest() throws InterruptedException {
        Matrix mtx = new Matrix(5, 5, true);
        mtx.set(3, 4, 5.0f);
        assertEquals(mtx.get(3, 4), 5.0f, 0.0000001);

        mtx.set(4, 1, 3.0f);
        assertEquals(mtx.get(4, 1), 3.0f, 0.0000001);

    }



    private boolean sameMatrixes(Matrix mtxA, Matrix mtxB){
        for(int i = 0; i < mtxA.getRows(); i++){
            for(int j = 0; j < mtxA.getCols(); j++){
                if(Math.abs(mtxA.get(i, j) - mtxB.get(i,j)) > 0.00001){
                    return false;
                }
            }
        }
        return true;
    }
}
