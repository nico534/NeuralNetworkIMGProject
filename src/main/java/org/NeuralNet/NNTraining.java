package org.NeuralNet;

import org.apache.commons.lang3.ArrayUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.Matrixes.Matrix;

public class NNTraining {
    private Matrix[] allImputs;
    private Matrix[] allLabels;
    private int[] hiddenLayers;
    private File networkFile;
    private boolean newNetwork;
    private NeuralNetwork nn;

    public boolean setUp(){
        if(this.nn != null){
            return true;
        }
        if(this.newNetwork){
            if(this.hiddenLayers == null || getTestDataLength() == 0){
                return false;
            }
            int[] networSize = new int[this.hiddenLayers.length + 2];
            networSize[0] = this.allImputs[0].getRows();
            for(int i = 0; i < hiddenLayers.length; i++){
                networSize[i + 1] = hiddenLayers[i];
            }
            networSize[networSize.length - 1] = allLabels[0].getRows();
            this.nn = new NeuralNetwork(networSize);
        } else {
            try {
                nn = new NeuralNetwork(this.networkFile);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public void setTrainingFile(File fileToTrainOn){
        this.networkFile = fileToTrainOn;
        this.newNetwork = false;
    }

    public void setHiddenLayerse(int[] hiddenLayers){
        this.hiddenLayers = hiddenLayers;
        this.newNetwork = true;
    }


    public void loadTestData(File testDataFile) throws IOException {
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ",";
        List<Matrix> input = new ArrayList<Matrix>();
        List<Matrix> labels = new ArrayList<Matrix>();

        br = new BufferedReader(new FileReader(testDataFile));
        for (int i = 0; ; i++) {
            if ((line = br.readLine()) == null) {
                break;
            }
            String[] country = line.split(csvSplitBy);
            float[] labelRaw = new float[10];
            labelRaw[Integer.parseInt(country[0])] = 1.0f;
            float[] inputRaw = new float[country.length - 1];
            for (int j = 0; j < inputRaw.length; j++) {
                // to make it a value between 1 and 0
                inputRaw[j] = Float.parseFloat(country[j + 1]) / (float) 256;
            }
            input.add(new Matrix(inputRaw));
            labels.add(new Matrix(labelRaw));
        }

        this.allImputs = input.toArray(new Matrix[0]);
        this.allLabels = labels.toArray(new Matrix[0]);
    }

    public void startTestRoutine(int range,  double learningRate, boolean print) {

        int start = ThreadLocalRandom.current().nextInt(0, allImputs.length - range);
        nn.train(ArrayUtils.subarray(this.allImputs, start, start + range), ArrayUtils.subarray(this.allLabels, start, start + range),
                learningRate, print);
    }

    public int getTestDataLength(){
        if(allImputs != null) {
            return allImputs.length;
        } else
            return 0;
    }

    public NeuralNetwork getNN(){
        return this.nn;
    }

    public void clear(){
        this.nn = null;
    }
}