import org.Matrixes.Matrix;
import org.Matrixes.MatrixCalc;
import org.NeuralNet.NNTraining;
import org.NeuralNet.NeuralNetwork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class main {
    public static void main(String[] args) throws IOException {

        int row = 2;
        int col =4;

        float[] val = {1, 2, 3, 4, 5, 6, 7, 8};

        Matrix mtx = new Matrix(val, row);
        mtx.print();

        Matrix mtx2 = mtx.copy();
        mtx2.transpose();
        mtx2.print();

        Matrix mtx3 = MatrixCalc.multiply(mtx, mtx2);
        mtx3.print();



    }
}
